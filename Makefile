run: start-db
	@./gradlew run

start: start-db start-api
	@docker-compose logs -f api

stop:
	@docker-compose down

start-api:
	@./gradlew dockerBuild
	@docker-compose up -d api

start-db:
	@docker-compose up -d db cloudbeaver

clean-db:
	@./gradlew flywayClean

test:
	@./gradlew check --info

.PHONY: run start stop start-api start-db clean-db test