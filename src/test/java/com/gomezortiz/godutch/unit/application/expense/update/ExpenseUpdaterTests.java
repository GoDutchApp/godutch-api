package com.gomezortiz.godutch.unit.application.expense.update;

import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.expense.update.ExpenseUpdater;
import com.gomezortiz.godutch.application.expense.update.dto.ExpenseUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ExpenseUpdaterTests {

    private ExpenseUpdater updater;
    private ExpenseFinder finder;
    private UserFinder userFinder;
    private ExpenseRepository repository;
    private ArgumentCaptor<Expense> expenseCaptor;

    @BeforeEach
    private void setUp() {
        finder = mock(ExpenseFinder.class);
        userFinder = mock(UserFinder.class);
        repository = mock(ExpenseRepository.class);
        updater = new ExpenseUpdater(finder, userFinder, repository);
        expenseCaptor = ArgumentCaptor.forClass(Expense.class);
    }

    @Test
    void should_update_only_modified_value() {

        var expense = ExpenseMother.random().build();
        when(finder.findById(String.valueOf(expense.getId()))).thenReturn(expense);
        var expectedDescription = "Another";
        var request = new ExpenseUpdateRequest(String.valueOf(expense.getId()), expectedDescription, expense.getAmount(), String.valueOf(expense.getUserId()));

        updater.update(request);

        verify(repository).update(expenseCaptor.capture());
        var actual = expenseCaptor.getValue();
        assertEquals(expectedDescription, actual.getDescription());
    }

    @Test
    void should_update_paid_by() {

        var expense = ExpenseMother.random().build();
        when(finder.findById(String.valueOf(expense.getId()))).thenReturn(expense);
        var expectedPaidBy = UserMother.random().build();
        when(userFinder.findById(String.valueOf(expectedPaidBy.getId()))).thenReturn(expectedPaidBy);
        var request = new ExpenseUpdateRequest(String.valueOf(expense.getId()), expense.getDescription(), expense.getAmount(), String.valueOf(expectedPaidBy.getId()));

        updater.update(request);

        verify(repository).update(expenseCaptor.capture());
        var actual = expenseCaptor.getValue();
        assertEquals(expectedPaidBy, actual.getPaidBy());
    }
}
