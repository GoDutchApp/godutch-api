package com.gomezortiz.godutch.unit.application.group.delete;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.group.delete.GroupDeleter;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class GroupDeleterTests {

    private GroupDeleter deleter;
    private GroupFinder groupFinder;
    private GroupRepository repository;
    private ExpenseDeleter expenseDeleter;

    @BeforeEach
    private void setUp() {
        groupFinder = mock(GroupFinder.class);
        repository = mock(GroupRepository.class);
        expenseDeleter = mock(ExpenseDeleter.class);
        deleter = new GroupDeleter(groupFinder, repository, expenseDeleter);
    }

    @Test
    void should_delete_group() {

        var expected = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(expenseDeleter).deleteAllByGroup(expected);
        verify(repository).delete(expected);
    }
}
