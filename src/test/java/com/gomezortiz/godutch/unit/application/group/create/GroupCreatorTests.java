package com.gomezortiz.godutch.unit.application.group.create;

import com.gomezortiz.godutch.application.group.create.GroupCreator;
import com.gomezortiz.godutch.application.group.create.dto.GroupCreateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class GroupCreatorTests {

    private GroupCreator creator;
    private GroupRepository repository;

    @BeforeEach
    private void setUp() {
        repository = mock(GroupRepository.class);
        creator = new GroupCreator(repository);
    }

    @Test
    void should_create_group() {

        var expected = GroupMother.random().members(new HashSet<>()).build();
        var request = new GroupCreateRequest(String.valueOf(expected.getId()), expected.getName());

        creator.create(request);

        verify(repository).create(expected);
    }

    @Test
    void should_throw_if_group_already_exists() {

        var expected = GroupMother.random().build();
        var request = new GroupCreateRequest(String.valueOf(expected.getId()), expected.getName());
        when(repository.existsByName(request.getName())).thenReturn(true);

        var e = assertThrows(AlreadyExists.class, () -> creator.create(request));

        assertEquals(String.format(ExceptionConstants.GROUP_ALREADY_EXISTS, request.getName()), e.getMessage());
        verify(repository, never()).create(expected);
    }
}
