package com.gomezortiz.godutch.unit.application.expense.create;

import com.gomezortiz.godutch.application.expense.create.ExpenseCreator;
import com.gomezortiz.godutch.application.expense.create.dto.ExpenseCreateRequest;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ExpenseCreatorTests {

    private ExpenseCreator creator;
    private GroupFinder groupFinder;
    private UserFinder userFinder;
    private ExpenseRepository repository;

    @BeforeEach
    private void setUp() {
        groupFinder = mock(GroupFinder.class);
        userFinder = mock(UserFinder.class);
        repository = mock(ExpenseRepository.class);
        creator = new ExpenseCreator(groupFinder, userFinder, repository);
    }

    @Test
    void should_create_expense() {

        var expected = ExpenseMother.random().build();
        when(groupFinder.findById(String.valueOf(expected.getGroupId()))).thenReturn(expected.getGroup());
        when(userFinder.findById(String.valueOf(expected.getUserId()))).thenReturn(expected.getPaidBy());
        var request = new ExpenseCreateRequest(String.valueOf(expected.getId()), expected.getDescription(), expected.getAmount(), String.valueOf(expected.getGroupId()), String.valueOf(expected.getUserId()));

        creator.create(request);

        verify(repository).create(expected);
    }
}
