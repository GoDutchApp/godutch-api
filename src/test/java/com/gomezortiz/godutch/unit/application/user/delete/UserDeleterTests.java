package com.gomezortiz.godutch.unit.application.user.delete;

import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.user.delete.UserDeleter;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.user.UserMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class UserDeleterTests {

    private UserDeleter deleter;
    private UserFinder userFinder;
    private UserRepository repository;
    private GroupUpdater groupUpdater;

    @BeforeEach
    void setUp() {
        userFinder = mock(UserFinder.class);
        repository = mock(UserRepository.class);
        groupUpdater = mock(GroupUpdater.class);
        deleter = new UserDeleter(userFinder, repository, groupUpdater);
    }

    @Test
    void should_delete_user() {

        var expected = UserMother.random().build();
        when(userFinder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(groupUpdater).removeMemberFromEveryGroup(expected);
        verify(repository).delete(expected);
    }
}
