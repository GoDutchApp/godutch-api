package com.gomezortiz.godutch.unit.application.expense.delete;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ExpenseDeleterTests {

    private ExpenseDeleter deleter;
    private ExpenseFinder finder;
    private ExpenseRepository repository;
    private ArgumentCaptor<Expense> expenseCaptor;

    @BeforeEach
    private void setUp() {
        finder = mock(ExpenseFinder.class);
        repository = mock(ExpenseRepository.class);
        deleter = new ExpenseDeleter(finder, repository);
        expenseCaptor = ArgumentCaptor.forClass(Expense.class);
    }

    @Test
    void should_delete_expense() {

        var expected = ExpenseMother.random().build();
        when(finder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(repository).delete(expected);
    }

    @Test
    void should_delete_all_expenses_by_group() {

        var group = GroupMother.random().build();
        var expected = List.of(
                ExpenseMother.random().group(group).build(),
                ExpenseMother.random().group(group).build()
        );
        when(repository.findByGroup(group)).thenReturn(expected);

        deleter.deleteAllByGroup(group);

        verify(repository, times(expected.size())).delete(expenseCaptor.capture());
        assertTrue(expenseCaptor.getAllValues().containsAll(expected));
    }
}
