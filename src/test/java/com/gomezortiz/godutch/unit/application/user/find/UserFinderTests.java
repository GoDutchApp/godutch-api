package com.gomezortiz.godutch.unit.application.user.find;

import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.user.UserMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserFinderTests {

    private UserFinder finder;
    private UserRepository repository;

    @BeforeEach
    void setUp() {
        repository = mock(UserRepository.class);
        finder = new UserFinder(repository);
    }

    @Test
    void should_find_user_by_id() {

        var expected = UserMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.of(expected));

        var actual = finder.findById(String.valueOf(expected.getId()));

        assertEquals(expected, actual);
    }

    @Test
    void should_throw_if_user_not_found_by_id() {

        var expected = UserMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.empty());

        var e = assertThrows(NotFound.class, () -> finder.findById(String.valueOf(expected.getId())));

        assertEquals(String.format(ExceptionConstants.USER_NOT_FOUND_BY_ID, expected.getId()), e.getMessage());
    }

    @Test
    void should_find_user_by_email() {

        var expected = UserMother.random().build();
        when(repository.findByEmail(expected.getEmail())).thenReturn(Optional.of(expected));

        var actual = finder.findByEmail(expected.getEmail());

        assertEquals(expected, actual);
    }

    @Test
    void should_throw_if_user_not_found_by_email() {

        var expected = UserMother.random().build();
        when(repository.findByEmail(expected.getEmail())).thenReturn(Optional.empty());

        var e = assertThrows(NotFound.class, () -> finder.findByEmail(expected.getEmail()));

        assertEquals(String.format(ExceptionConstants.USER_NOT_FOUND_BY_EMAIL, expected.getEmail()), e.getMessage());
    }
}
