package com.gomezortiz.godutch.unit.domain.group;

import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class GroupTests {

    @Test
    void should_create() {

        var expected = GroupMother.random().members(new HashSet<>()).build();

        var actual = Group.create(expected.getId(), expected.getName());

        assertEquals(expected, actual);
    }

    @Test
    void should_add_member() {

        var group = GroupMother.random().members(new HashSet<>()).build();
        var member = UserMother.random().build();

        var actual = group.addMember(member);

        assertTrue(actual.getMembers().contains(member));
    }

    @Test
    void should_remove_member() {

        var member = UserMother.random().build();
        var group = GroupMother.random().members(Collections.singleton(member)).build();

        var actual = group.removeMember(member);

        assertFalse(actual.getMembers().contains(member));
    }
}
