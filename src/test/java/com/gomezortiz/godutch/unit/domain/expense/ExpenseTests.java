package com.gomezortiz.godutch.unit.domain.expense;

import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExpenseTests {

    @Test
    void should_create() {

        var expected = ExpenseMother.random().build();

        var actual = Expense.create(expected.getId(), expected.getDescription(), expected.getAmount(), expected.getGroup(), expected.getPaidBy());

        assertEquals(expected, actual);
    }
}
