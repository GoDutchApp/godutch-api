package com.gomezortiz.godutch.unit.domain.user;

import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.user.UserMother;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTests {

    @Test
    void should_create() {

        var expected = UserMother.random().build();

        var actual = User.create(expected.getId(), expected.getFirstName(), expected.getLastName1(), expected.getLastName2(), expected.getEmail());

        assertEquals(expected, actual);
    }
}
