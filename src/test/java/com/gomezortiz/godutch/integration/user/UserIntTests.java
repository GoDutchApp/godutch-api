package com.gomezortiz.godutch.integration.user;

import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.user.create.UserCreator;
import com.gomezortiz.godutch.application.user.create.dto.UserCreateRequest;
import com.gomezortiz.godutch.application.user.delete.UserDeleter;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.application.user.update.UserUpdater;
import com.gomezortiz.godutch.application.user.update.dto.UserUpdateRequest;
import com.gomezortiz.godutch.common.IntegrationTest;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.test.annotation.MockBean;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserIntTests extends IntegrationTest {

    @Inject
    private UserFinder finder;

    @Inject
    private UserCreator creator;

    @Inject
    private UserUpdater updater;

    @Inject
    private UserDeleter deleter;

    @Inject
    private UserRepository repository;

    @Inject
    private GroupUpdater groupUpdater;

    private User user;
    private Group group;

    @BeforeEach
    private void setUp() {
        user = UserMother.random().build();
        em.persist(user);
        group = GroupMother.random().members(Collections.singleton(user)).build();
        em.persist(group);
    }

    @Test
    void should_find_by_id() {

        var expected = UserResponse.fromUser(user);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/users/" + user.getId()), UserResponse.class);

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findById(String.valueOf(user.getId()));
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_find_by_email() {

        var expected = UserResponse.fromUser(user);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/users/email/" + user.getEmail()), UserResponse.class);

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findByEmail(user.getEmail());
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_create() {

        var user = UserMother.random().build();
        var request = new UserCreateRequest(String.valueOf(user.getId()), user.getFirstName(), user.getLastName1(), user.getLastName2(), user.getEmail());

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.PUT("/api/users", UserCreateRequest.class).body(request));

        assertEquals(HttpStatus.CREATED, res.getStatus());
        assertEquals("/api/users/" + user.getId(), res.getHeaders().get("location"));
        verify(creator).create(request);
        var foundUser = em.find(User.class, user.getId());
        assertEquals(user, foundUser);
    }

    @Test
    void should_update() {

        var expectedEmail = "another@email.com";
        var request = new UserUpdateRequest(String.valueOf(user.getId()), user.getFirstName(), user.getLastName1(), user.getLastName2(), expectedEmail);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.PATCH("/api/users", UserUpdateRequest.class).body(request));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(updater).update(request);
        var foundUser = em.find(User.class, user.getId());
        assertEquals(expectedEmail, foundUser.getEmail());
    }

    @Test
    void should_delete() {

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.DELETE("/api/users/" + user.getId()));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(deleter).delete(String.valueOf(user.getId()));
        var foundUser = em.find(User.class, user.getId());
        assertNull(foundUser);
        var foundGroup = em.find(Group.class, group.getId());
        assertFalse(foundGroup.getMembers().contains(user));
    }

    @MockBean(UserFinder.class)
    UserFinder finder() {
        return mock(UserFinder.class, withSettings().useConstructor(repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(UserCreator.class)
    UserCreator creator() {
        return mock(UserCreator.class, withSettings().useConstructor(repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(UserUpdater.class)
    UserUpdater updater() {
        return mock(UserUpdater.class, withSettings().useConstructor(finder, repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(UserDeleter.class)
    UserDeleter deleter() {
        return mock(UserDeleter.class, withSettings().useConstructor(finder, repository, groupUpdater).defaultAnswer(CALLS_REAL_METHODS));
    }
}
