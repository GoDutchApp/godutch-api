package com.gomezortiz.godutch.integration.expense;

import com.gomezortiz.godutch.application.expense.balance.ExpenseBalanceCalculator;
import com.gomezortiz.godutch.application.expense.balance.dto.ExpenseBalanceResponse;
import com.gomezortiz.godutch.application.expense.balance.dto.PaymentResponse;
import com.gomezortiz.godutch.application.expense.balance.dto.UserBalanceResponse;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.common.IntegrationTest;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.test.annotation.MockBean;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ExpenseBalanceIntTests extends IntegrationTest {

    @Inject
    private ExpenseBalanceCalculator balanceCalculator;

    @Inject
    private ExpenseFinder finder;

    @Inject
    private GroupFinder groupFinder;

    private String groupId;

    @BeforeEach
    private void setUp() {
        var members = mockMembers();
        var group = mockGroup(members);
        groupId = String.valueOf(group.getId());
        mockExpenses(group, members);
    }

    @Test
    void should_calculate_balance_for_a_group() {

        var expectedBalanceAmounts = List.of(-59.15D, -22.55D, 40.85D, 40.85D);
        var expectedPaymentAmounts = List.of(40.85D, 22.55D, 18.30D);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/expenses/group/balance/" + groupId), ExpenseBalanceResponse.class);

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(balanceCalculator).calculateBalance(String.valueOf(groupId));
        var body = res.getBody();
        assertTrue(body.isPresent());
        var actual = body.get();
        assertEquals(
                expectedBalanceAmounts,
                actual.getBalance().stream()
                        .map(UserBalanceResponse::getBalance)
                        .map(amount -> new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue())
                        .collect(Collectors.toList())
        );
        assertEquals(
                expectedPaymentAmounts,
                actual.getPayments().stream()
                        .map(PaymentResponse::getAmount)
                        .map(amount -> new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue())
                        .collect(Collectors.toList())
        );
    }

    private void mockExpenses(Group group, List<User> members) {
        List<Expense> expenses = List.of(
                ExpenseMother.random().group(group).paidBy(members.get(0)).amount(100D).build(),
                ExpenseMother.random().group(group).paidBy(members.get(1)).amount(10D).build(),
                ExpenseMother.random().group(group).paidBy(members.get(1)).amount(53.40D).build()
        );
        expenses.forEach(expense -> em.persist(expense));
    }

    private Group mockGroup(List<User> members) {
        Group group = GroupMother.random().members(new HashSet<>(members)).build();
        em.persist(group);
        return group;
    }

    private List<User> mockMembers() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            users.add(UserMother.random().build());
        }
        users.forEach(user -> em.persist(user));
        return users;
    }

    @MockBean(ExpenseBalanceCalculator.class)
    ExpenseBalanceCalculator balanceCalculator() {
        return mock(ExpenseBalanceCalculator.class, withSettings().useConstructor(groupFinder, finder).defaultAnswer(CALLS_REAL_METHODS));
    }
}
