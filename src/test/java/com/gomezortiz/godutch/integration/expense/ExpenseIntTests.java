package com.gomezortiz.godutch.integration.expense;

import com.gomezortiz.godutch.application.expense.create.ExpenseCreator;
import com.gomezortiz.godutch.application.expense.create.dto.ExpenseCreateRequest;
import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.expense.update.ExpenseUpdater;
import com.gomezortiz.godutch.application.expense.update.dto.ExpenseUpdateRequest;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.common.IntegrationTest;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import com.gomezortiz.godutch.rest.expense.dto.ExpenseResponse;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.test.annotation.MockBean;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExpenseIntTests extends IntegrationTest {

    @Inject
    private ExpenseFinder finder;

    @Inject
    private ExpenseCreator creator;

    @Inject
    private ExpenseUpdater updater;

    @Inject
    private ExpenseDeleter deleter;

    @Inject
    private ExpenseRepository repository;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private GroupRepository groupRepository;

    @Inject
    private UserFinder userFinder;

    @Inject
    private UserRepository userRepository;

    private Expense expense;
    private Group group;
    private User user;

    @BeforeEach
    private void setUp() {
        user = UserMother.random().build();
        em.persist(user);
        group = GroupMother.random().members(Collections.singleton(user)).build();
        em.persist(group);
        expense = ExpenseMother.random().group(group).paidBy(user).build();
        em.persist(expense);
    }

    @Test
    void should_find_by_id() {

        var expected = ExpenseResponse.fromExpense(expense);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/expenses/" + expense.getId()), ExpenseResponse.class);

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findById(String.valueOf(expense.getId()));
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_find_by_group() {

        var expected = Collections.singleton(ExpenseResponse.fromExpense(expense));

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/expenses/group/" + group.getId()), Argument.of(Set.class, ExpenseResponse.class));

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findByGroup(String.valueOf(group.getId()));
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_create() {

        var expense = ExpenseMother.random().group(group).paidBy(user).build();
        var request = new ExpenseCreateRequest(String.valueOf(expense.getId()), expense.getDescription(), expense.getAmount(), String.valueOf(expense.getGroupId()), String.valueOf(expense.getUserId()));

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.PUT("/api/expenses", ExpenseCreateRequest.class).body(request));

        assertEquals(HttpStatus.CREATED, res.getStatus());
        assertEquals("/api/expenses/" + expense.getId(), res.getHeaders().get("location"));
        verify(creator).create(request);
        var foundExpense = em.find(Expense.class, expense.getId());
        assertEquals(expense, foundExpense);
    }

    @Test
    void should_update() {

        var expectedDescription = "Another";
        var request = new ExpenseUpdateRequest(String.valueOf(expense.getId()), expectedDescription, expense.getAmount(), String.valueOf(expense.getUserId()));

        var res = httpClient.toBlocking().exchange(HttpRequest.PATCH("/api/expenses", ExpenseUpdateRequest.class).body(request));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(updater).update(request);
        var foundExpense = em.find(Expense.class, expense.getId());
        assertEquals(expectedDescription, foundExpense.getDescription());
    }

    @Test
    void should_delete() {

        var res = httpClient.toBlocking().exchange(HttpRequest.DELETE("/api/expenses/" + expense.getId()));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(deleter).delete(String.valueOf(expense.getId()));
        var foundExpense = em.find(Expense.class, expense.getId());
        assertNull(foundExpense);
    }

    @MockBean(ExpenseFinder.class)
    ExpenseFinder finder() {
        return mock(ExpenseFinder.class, withSettings().useConstructor(groupFinder, repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(ExpenseCreator.class)
    ExpenseCreator creator() {
        return mock(ExpenseCreator.class, withSettings().useConstructor(groupFinder, userFinder, repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(ExpenseUpdater.class)
    ExpenseUpdater updater() {
        return mock(ExpenseUpdater.class, withSettings().useConstructor(finder, userFinder, repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(ExpenseDeleter.class)
    ExpenseDeleter deleter() {
        return mock(ExpenseDeleter.class, withSettings().useConstructor(finder, repository).defaultAnswer(CALLS_REAL_METHODS));
    }
}
