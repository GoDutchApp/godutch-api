package com.gomezortiz.godutch.common;

import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@MicronautTest
public abstract class IntegrationTest {

    @Inject
    @Client("/")
    protected HttpClient httpClient;

    @PersistenceContext
    protected EntityManager em;
}
