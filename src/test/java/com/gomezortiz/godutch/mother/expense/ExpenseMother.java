package com.gomezortiz.godutch.mother.expense;

import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.common.MotherCreator;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;

import java.time.LocalDateTime;
import java.util.UUID;

public final class ExpenseMother {

    private static Expense.ExpenseBuilder create(UUID id, String description, Double amount, Group group, User paidBy, LocalDateTime paidAt) {
        return Expense.builder()
                .id(id)
                .description(description)
                .amount(amount)
                .group(group)
                .paidBy(paidBy)
                .paidAt(paidAt);
    }

    public static Expense.ExpenseBuilder random() {
        return create(
                UUID.randomUUID(),
                MotherCreator.random().lorem().characters(10, 255),
                MotherCreator.random().number().randomDouble(2, 10, 100),
                GroupMother.random().build(),
                UserMother.random().build(),
                LocalDateTime.now()
        );
    }
}
