package com.gomezortiz.godutch.mother.user;

import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.common.MotherCreator;

import java.util.UUID;

public final class UserMother {

    private static User.UserBuilder create(UUID id, String firstName, String lastName1, String lastName2, String email) {
        return User.builder()
                .id(id)
                .firstName(firstName)
                .lastName1(lastName1)
                .lastName2(lastName2)
                .email(email);
    }

    public static User.UserBuilder random() {
        return create(
                UUID.randomUUID(),
                MotherCreator.random().name().firstName(),
                MotherCreator.random().name().lastName(),
                MotherCreator.random().name().lastName(),
                MotherCreator.random().internet().emailAddress()
        );
    }
}
