package com.gomezortiz.godutch.mother.group;

import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.common.MotherCreator;
import com.gomezortiz.godutch.mother.user.UserMother;

import java.util.Set;
import java.util.UUID;

public final class GroupMother {

    private static Group.GroupBuilder create(UUID id, String name, Set<User> members) {
        return Group.builder()
                .id(id)
                .name(name)
                .members(members);
    }

    public static Group.GroupBuilder random() {
        return create(
                UUID.randomUUID(),
                MotherCreator.random().lorem().characters(10, 255),
                Set.of(UserMother.random().build())
        );
    }
}
