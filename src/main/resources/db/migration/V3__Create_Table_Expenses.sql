CREATE TABLE expenses (
	id uuid NOT NULL,
	amount float8 NOT NULL,
	description varchar(255) NOT NULL,
	paid_at timestamp NOT NULL,
	group_id uuid NOT NULL,
	user_id uuid NOT NULL,
	PRIMARY KEY (id)
);

ALTER TABLE expenses ADD CONSTRAINT fkhpk0n2cbnfiuu5nrgl0ika3hq FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE expenses ADD CONSTRAINT fkne4jfgy6h5e1gv78elj8bypb5 FOREIGN KEY (group_id) REFERENCES "groups"(id);