CREATE TABLE "groups" (
	id uuid NOT NULL,
	"name" varchar(255) NOT NULL,
	PRIMARY KEY (id),
    UNIQUE ("name")
);