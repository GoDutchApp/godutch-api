package com.gomezortiz.godutch.application.user.find;

import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserFinder {

    private final UserRepository repository;

    @Transactional
    public User findById(String id) {
        return repository.findById(UUID.fromString(id))
                .orElseThrow(() -> new NotFound(ExceptionConstants.USER_NOT_FOUND_BY_ID, id));
    }

    @Transactional
    public User findByEmail(String email) {
        return repository.findByEmail(email)
                .orElseThrow(() -> new NotFound(ExceptionConstants.USER_NOT_FOUND_BY_EMAIL, email));
    }
}
