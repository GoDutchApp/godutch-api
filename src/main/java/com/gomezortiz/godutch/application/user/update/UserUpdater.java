package com.gomezortiz.godutch.application.user.update;

import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.application.user.update.dto.UserUpdateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserUpdater {

    private final UserFinder finder;
    private final UserRepository repository;

    @Transactional
    public void update(UserUpdateRequest request) {

        User user = finder.findById(request.getId());

        String firstName = request.getFirstName();
        if(StringUtils.isNotBlank(firstName) && !StringUtils.equals(user.getFirstName(), firstName)) {
            user = user.withFirstName(firstName);
        }

        String lastName1 = request.getLastName1();
        if(StringUtils.isNotBlank(lastName1) && !StringUtils.equals(user.getLastName1(), lastName1)) {
            user = user.withLastName1(lastName1);
        }

        String lastName2 = request.getLastName2();
        if(StringUtils.isNotBlank(lastName2) && !StringUtils.equals(user.getLastName2(), lastName2)) {
            user = user.withLastName2(lastName2);
        }

        String email = request.getEmail();
        if(StringUtils.isNotBlank(email) && !StringUtils.equals(user.getEmail(), email)) {
            if (repository.existsByEmail(request.getEmail())) {
                throw new AlreadyExists(ExceptionConstants.USER_ALREADY_EXISTS, request.getEmail());
            }
            user = user.withEmail(email);
        }

        repository.update(user);
        log.debug("User updated: {}", user);
    }
}
