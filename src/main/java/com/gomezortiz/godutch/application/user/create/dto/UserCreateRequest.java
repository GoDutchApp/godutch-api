package com.gomezortiz.godutch.application.user.create.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud de creación de un usuario")
public class UserCreateRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del usuario")
    private String id;

    @NotBlank
    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Nombre de pila del usuario")
    private String firstName;

    @NotBlank
    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Primer apellido del usuario")
    private String lastName1;

    @Schema(description = "Segundo apellido del usuario")
    private String lastName2;

    @Email
    @NotBlank
    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Correo electrónico del usuario")
    private String email;

    public User toUser() {
        return User.create(UUID.fromString(id), firstName, lastName1, lastName2, email);
    }
}
