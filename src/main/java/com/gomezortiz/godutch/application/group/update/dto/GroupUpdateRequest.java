package com.gomezortiz.godutch.application.group.update.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud de actualización de un grupo")
public class GroupUpdateRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del grupo")
    private String id;

    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Nombre del grupo")
    private String name;
}
