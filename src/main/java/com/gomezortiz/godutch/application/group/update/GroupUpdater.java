package com.gomezortiz.godutch.application.group.update;

import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateMembersRequest;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.domain.user.User;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class GroupUpdater {

    private final GroupFinder finder;
    private final GroupRepository repository;
    private final UserFinder userFinder;

    @Transactional
    public void update(GroupUpdateRequest request) {

        Group group = finder.findById(request.getId());

        String name = request.getName();
        if (StringUtils.isNotBlank(name) && !StringUtils.equals(group.getName(), name)) {
            if (repository.existsByName(request.getName())) {
                throw new AlreadyExists(ExceptionConstants.GROUP_ALREADY_EXISTS, request.getName());
            }
            group = group.withName(name);
        }

        repository.update(group);
        log.debug("Group updated: {}", group);
    }

    @Transactional
    public void addMember(GroupUpdateMembersRequest request) {
        Group group = finder.findById(request.getGroupId());
        User member = userFinder.findById(request.getUserId());
        repository.update(group.addMember(member));
        log.debug("User {} added to group: {}", member.getId(), group);
    }

    @Transactional
    public void removeMember(GroupUpdateMembersRequest request) {
        Group group = finder.findById(request.getGroupId());
        User member = userFinder.findById(request.getUserId());
        repository.update(group.removeMember(member));
        log.debug("User {} removed from group: {}", member.getId(), group);
    }

    @Transactional
    public void removeMemberFromEveryGroup(User member) {
        repository.findByMember(member).forEach(group -> {
            repository.update(group.removeMember(member));
            log.debug("User {} removed from group: {}", member.getId(), group);
        });
    }
}
