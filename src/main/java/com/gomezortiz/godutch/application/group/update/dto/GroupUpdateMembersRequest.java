package com.gomezortiz.godutch.application.group.update.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud para añadir o eliminar miembros de un grupo")
public class GroupUpdateMembersRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del grupo")
    private String groupId;

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del usuario a añadir o eliminar")
    private String userId;
}
