package com.gomezortiz.godutch.application.group.create;

import com.gomezortiz.godutch.application.group.create.dto.GroupCreateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class GroupCreator {

    private final GroupRepository repository;

    @Transactional
    public void create(GroupCreateRequest request) {

        if (repository.existsByName(request.getName())) {
            throw new AlreadyExists(ExceptionConstants.GROUP_ALREADY_EXISTS, request.getName());
        }

        Group created = repository.create(request.toGroup());
        log.debug("New group created: {}", created);
    }
}
