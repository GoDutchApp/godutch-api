package com.gomezortiz.godutch.application.group.create.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud de creación de un grupo")
public class GroupCreateRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del grupo")
    private String id;

    @NotBlank
    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Nombre del grupo")
    private String name;

    public Group toGroup() {
        return Group.create(UUID.fromString(id), name);
    }
}
