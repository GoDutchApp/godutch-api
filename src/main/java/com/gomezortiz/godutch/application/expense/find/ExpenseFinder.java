package com.gomezortiz.godutch.application.expense.find;

import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.group.Group;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class ExpenseFinder {

    private final GroupFinder groupFinder;
    private final ExpenseRepository repository;

    @Transactional
    public Expense findById(String id) {
        return repository.findById(UUID.fromString(id))
                .orElseThrow(() -> new NotFound(ExceptionConstants.EXPENSE_NOT_FOUND, id));
    }

    @Transactional
    public List<Expense> findByGroup(String groupId) {
        Group group = groupFinder.findById(groupId);
        return repository.findByGroup(group);
    }
}
