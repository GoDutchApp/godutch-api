package com.gomezortiz.godutch.application.expense.balance.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Resultado del cálculo del balance de gastos de un grupo")
public class ExpenseBalanceResponse {

    @Schema(description = "Balance particular de un miembro del grupo")
    private List<UserBalanceResponse> balance;

    @Schema(description = "Pagos necesarios para equilibrar el balance del grupo")
    private List<PaymentResponse> payments;

    public List<PaymentResponse> processPayments() {
        while (hasDebt()) {
            processNextPayment();
        }
        return payments;
    }

    public void processNextPayment() {
        UserBalanceResponse maxCreditor = getMaxCreditor();
        UserBalanceResponse maxDebtor = getMaxDebtor();
        double maxCreditorDebt = Math.abs(maxCreditor.getBalance());
        if (maxCreditorDebt >= maxDebtor.getBalance()) {
            payments.add(new PaymentResponse(maxDebtor.getUser(), maxCreditor.getUser(), maxDebtor.getBalance()));
            maxCreditor.setBalance(maxCreditor.getBalance() + maxDebtor.getBalance());
            maxDebtor.setBalance(0D);
        } else {
            payments.add(new PaymentResponse(maxDebtor.getUser(), maxCreditor.getUser(), maxCreditorDebt));
            maxDebtor.setBalance(maxDebtor.getBalance() - maxCreditorDebt);
            maxCreditor.setBalance(0D);
        }
    }

    public boolean hasDebt() {
        return getMaxCreditor().getBalance() < 0;
    }

    public UserBalanceResponse getMaxDebtor() {
        return balance.stream().max(Comparator.comparing(UserBalanceResponse::getBalance)).orElse(null);
    }

    public UserBalanceResponse getMaxCreditor() {
        return balance.stream().min(Comparator.comparing(UserBalanceResponse::getBalance)).orElse(null);
    }

    public ExpenseBalanceResponse sorted() {
        return new ExpenseBalanceResponse(
                balance.stream()
                        .sorted(Comparator.comparing(UserBalanceResponse::getBalance))
                        .collect(Collectors.toList()),
                payments
        );
    }

    public ExpenseBalanceResponse processExpenses(List<Expense> expenses) {
        expenses.forEach(expense -> {
            double share = expense.getAmount() / numOfUsers();
            balance.forEach(userBalance -> userBalance.processExpense(expense, share));
        });
        return this;
    }

    public int numOfUsers() {
        return balance.size();
    }

    public static ExpenseBalanceResponse fromUsers(Set<User> users) {
        return new ExpenseBalanceResponse(
                users.stream().map(UserBalanceResponse::fromUser).collect(Collectors.toList()),
                new ArrayList<>()
        );
    }
}
