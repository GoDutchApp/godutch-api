package com.gomezortiz.godutch.application.expense.balance.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Resultado del cálculo del balance de gastos de un usuario")
public class UserBalanceResponse {

    @Schema(description = "Usuario al que se asocia el balance")
    private UserResponse user;

    @Schema(description = "Balance de gastos del usuario")
    private Double balance;

    public void processExpense(Expense expense, double share) {
        if(hasPaid(expense)) {
            balance -= expense.getAmount() - share;
        } else {
            balance += share;
        }
    }

    public boolean hasPaid(Expense expense) {
        return StringUtils.equals(user.getId(), String.valueOf(expense.getPaidBy().getId()));
    }

    public static UserBalanceResponse fromUser(User user) {
        return new UserBalanceResponse(
                UserResponse.fromUser(user),
                0D
        );
    }
}
