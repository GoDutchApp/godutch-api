package com.gomezortiz.godutch.application.expense.create.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud de creación de un gasto")
public class ExpenseCreateRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del gasto")
    private String id;

    @NotBlank
    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Descripción del gasto")
    private String description;

    @NotNull
    @Schema(description = "Importe del gasto")
    private Double amount;

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del grupo asociado al gasto")
    private String groupId;

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del usuario que realiza el pago")
    private String paidBy;

    public Expense toExpense(Group group, User paidBy) {
        return Expense.create(UUID.fromString(id), description, amount, group, paidBy);
    }
}
