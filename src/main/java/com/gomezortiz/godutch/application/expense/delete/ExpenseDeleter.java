package com.gomezortiz.godutch.application.expense.delete;

import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.group.Group;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ExpenseDeleter {

    private final ExpenseFinder finder;
    private final ExpenseRepository repository;

    @Transactional
    public void delete(String id) {
        Expense expense = finder.findById(id);
        repository.delete(expense);
        log.debug("Expense deleted: {}", expense);
    }

    @Transactional
    public void deleteAllByGroup(Group group) {
        repository.findByGroup(group).forEach(expense -> {
            repository.delete(expense);
            log.debug("Expense deleted: {}", expense);
        });
    }
}
