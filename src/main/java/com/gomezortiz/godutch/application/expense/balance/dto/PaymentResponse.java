package com.gomezortiz.godutch.application.expense.balance.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Pago necesario para equilibrar el balance de gastos de un grupo")
public class PaymentResponse {

    @Schema(description = "Usuario que debe realizar el pago")
    private UserResponse paidBy;

    @Schema(description = "Usuario que debe recibir el pago")
    private UserResponse paidTo;

    @Schema(description = "Usuario al que se asocia el balance")
    private Double amount;
}
