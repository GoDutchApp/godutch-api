package com.gomezortiz.godutch.rest.common.swagger;

import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({METHOD, TYPE, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponse(responseCode = SwaggerConstants.NO_CONTENT_CODE, description = SwaggerConstants.NO_CONTENT_DESCRIPTION)
public @interface NoContentRes {
}
