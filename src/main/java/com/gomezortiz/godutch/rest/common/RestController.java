package com.gomezortiz.godutch.rest.common;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.uri.UriBuilder;

import java.net.URI;
import java.util.Map;

public abstract class RestController {

    protected static final String ID_PATH = "/{id}";

    protected final HttpResponse<Void> createdResponse(HttpRequest<?> httpRequest, String id) {
        URI uri = UriBuilder.of(httpRequest.getUri() + ID_PATH).expand(Map.of("id", id));
        return HttpResponse.created(uri);
    }
}
