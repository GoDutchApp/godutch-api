package com.gomezortiz.godutch.rest.user;

import com.gomezortiz.godutch.application.user.create.UserCreator;
import com.gomezortiz.godutch.application.user.create.dto.UserCreateRequest;
import com.gomezortiz.godutch.application.user.delete.UserDeleter;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.application.user.update.UserUpdater;
import com.gomezortiz.godutch.application.user.update.dto.UserUpdateRequest;
import com.gomezortiz.godutch.rest.common.RestController;
import com.gomezortiz.godutch.rest.common.swagger.*;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

@Validated
@Controller("/api/users")
@RequiredArgsConstructor
@Tag(name = "Users", description = "Endpoints to perform operations with users")
public class UserController extends RestController {

    private final UserFinder finder;
    private final UserCreator creator;
    private final UserUpdater updater;
    private final UserDeleter deleter;

    @Get(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find a user by its unique id", description = "Find a user by its unique id")
    public HttpResponse<UserResponse> findById(@PathVariable String id) {
        return HttpResponse.ok(UserResponse.fromUser(finder.findById(id)));
    }

    @Get(value = "email/{email}", produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find a user by its unique email", description = "Find a user by its unique email")
    public HttpResponse<UserResponse> findByEmail(@PathVariable String email) {
        return HttpResponse.ok(UserResponse.fromUser(finder.findByEmail(email)));
    }

    @Put(produces = MediaType.APPLICATION_JSON)
    @CreatedRes
    @ErrorRes
    @Operation(summary = "Create a user", description = "Create a user")
    public HttpResponse<Void> create(HttpRequest<?> httpRequest, @Valid @Body UserCreateRequest request) {
        creator.create(request);
        return createdResponse(httpRequest, request.getId());
    }

    @Patch(produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Update a given user", description = "Update a given user")
    public HttpResponse<Void> update(@Valid @Body UserUpdateRequest request) {
        updater.update(request);
        return HttpResponse.noContent();
    }

    @Delete(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Delete a given user", description = "Delete a given user")
    public HttpResponse<Void> delete(@PathVariable String id) {
        deleter.delete(id);
        return HttpResponse.noContent();
    }
}
