package com.gomezortiz.godutch.rest.group;

import com.gomezortiz.godutch.application.group.create.GroupCreator;
import com.gomezortiz.godutch.application.group.create.dto.GroupCreateRequest;
import com.gomezortiz.godutch.application.group.delete.GroupDeleter;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateMembersRequest;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateRequest;
import com.gomezortiz.godutch.rest.common.RestController;
import com.gomezortiz.godutch.rest.common.swagger.*;
import com.gomezortiz.godutch.rest.group.dto.GroupResponse;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@Validated
@Controller("/api/groups")
@RequiredArgsConstructor
@Tag(name = "Groups", description = "Endpoints to perform operations with groups")
public class GroupController extends RestController {

    private final GroupFinder finder;
    private final GroupCreator creator;
    private final GroupUpdater updater;
    private final GroupDeleter deleter;

    @Get(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find a group by its unique id", description = "Find a group by its unique id")
    public HttpResponse<GroupResponse> findById(@PathVariable String id) {
        return HttpResponse.ok(GroupResponse.fromGroup(finder.findById(id)));
    }

    @Get(value = "/member" + ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find groups whose members include one with a given unique ID", description = "Find groups whose members include one with a given unique ID")
    public HttpResponse<Set<GroupResponse>> findByMember(@PathVariable String id) {
        return HttpResponse.ok(
                finder.findByMember(id)
                        .stream()
                        .map(GroupResponse::fromGroup)
                        .collect(Collectors.toSet())
        );
    }

    @Put(produces = MediaType.APPLICATION_JSON)
    @CreatedRes
    @ErrorRes
    @Operation(summary = "Create a group", description = "Create a group")
    public HttpResponse<Void> create(HttpRequest<?> httpRequest, @Valid @Body GroupCreateRequest request) {
        creator.create(request);
        return createdResponse(httpRequest, request.getId());
    }

    @Patch(produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Update a given group", description = "Update a given group")
    public HttpResponse<Void> update(@Valid @Body GroupUpdateRequest request) {
        updater.update(request);
        return HttpResponse.noContent();
    }

    @Patch(value = "/member/add", produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Add a member to a given group", description = "Add a member to a given group")
    public HttpResponse<Void> addMember(@Valid @Body GroupUpdateMembersRequest request) {
        updater.addMember(request);
        return HttpResponse.noContent();
    }

    @Patch(value = "/member/remove", produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Remove a member from a given group", description = "Remove a member from a given group")
    public HttpResponse<Void> removeMember(@Valid @Body GroupUpdateMembersRequest request) {
        updater.removeMember(request);
        return HttpResponse.noContent();
    }

    @Delete(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Delete a given group", description = "Delete a given group")
    public HttpResponse<Void> delete(@PathVariable String id) {
        deleter.delete(id);
        return HttpResponse.noContent();
    }
}
