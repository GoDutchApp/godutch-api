package com.gomezortiz.godutch.rest.group.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.stream.Collectors;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Grupo que cumple con el criterio de búsqueda")
public class GroupResponse {

    @Schema(description = "Identificador único del grupo")
    private String id;

    @Schema(description = "Nombre del grupo")
    private String name;

    @Schema(description = "Miembros del grupo")
    private Set<UserResponse> members;

    public static GroupResponse fromGroup(Group group) {
        return group != null
                ? new GroupResponse(
                String.valueOf(group.getId()),
                group.getName(),
                group.getMembers()
                        .stream()
                        .map(UserResponse::fromUser)
                        .collect(Collectors.toSet()))
                : null;
    }
}
