package com.gomezortiz.godutch.rest.expense.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.rest.group.dto.GroupResponse;
import com.gomezortiz.godutch.rest.user.dto.UserResponse;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Gasto que cumple con el criterio de búsqueda")
public class ExpenseResponse {

    @Schema(description = "Identificador único del gasto")
    private String id;

    @Schema(description = "Descripción del gasto")
    private String description;

    @Schema(description = "Importe del gasto")
    private Double amount;

    @Schema(description = "Grupo asociado al gasto")
    private GroupResponse group;

    @Schema(description = "Usuario que realiza el pago")
    private UserResponse paidBy;

    @Schema(description = "Fecha en la que se produjo el pago")
    private LocalDateTime paidAt;

    public static ExpenseResponse fromExpense(Expense expense) {
        return new ExpenseResponse(
                String.valueOf(expense.getId()),
                expense.getDescription(),
                expense.getAmount(),
                GroupResponse.fromGroup(expense.getGroup()),
                UserResponse.fromUser(expense.getPaidBy()),
                expense.getPaidAt()
        );
    }
}
