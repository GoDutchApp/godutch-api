package com.gomezortiz.godutch.domain.expense;

import com.gomezortiz.godutch.domain.group.Group;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ExpenseRepository {
    Optional<Expense> findById(UUID id);
    List<Expense> findByGroup(Group group);
    Expense create(Expense expense);
    Expense update(Expense expense);
    void delete(Expense expense);
}
