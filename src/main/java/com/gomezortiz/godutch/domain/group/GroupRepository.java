package com.gomezortiz.godutch.domain.group;

import com.gomezortiz.godutch.domain.user.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GroupRepository {
    Optional<Group> findById(UUID id);
    List<Group> findByMember(User member);
    boolean existsByName(String name);
    Group create(Group group);
    Group update(Group group);
    void delete(Group group);
}
