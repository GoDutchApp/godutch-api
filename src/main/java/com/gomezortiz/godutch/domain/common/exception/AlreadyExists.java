package com.gomezortiz.godutch.domain.common.exception;

public class AlreadyExists extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AlreadyExists(String message, String id) {
        super(String.format(message, id));
    }
}