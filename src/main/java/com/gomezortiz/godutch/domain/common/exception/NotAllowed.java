package com.gomezortiz.godutch.domain.common.exception;

public class NotAllowed extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotAllowed(String message) {
        super(message);
    }
}
