package com.gomezortiz.godutch.domain.common.exception.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class ExceptionConstants {

    // EXCEPTIONS
    public static final String NOT_FOUND = "The requested content cannot be found";
    public static final String NOT_VALID = "The request is not valid";
    public static final String ALREADY_EXISTS = "The requested content already exists and cannot be created";
    public static final String NOT_ALLOWED = "The requested operation is not allowed";

    // COMMON VALIDATION
    public static final String REQUIRED_ARGUMENT = "%s is required";
    public static final String LENGTH_ABOVE_MAX = "Must not be more than {max} characters long";
    public static final String LENGTH_BELOW_MIN = "Must not be less than {min} characters long";
    public static final String LENGTH_NOT_VALID = "Must be between {min} and {max} characters long";
    public static final String SIZE_ABOVE_MAX = "Value must not be less than {min}";
    public static final String SIZE_BELOW_MIN = "Value must not be less than {min}";
    public static final String SIZE_NOT_VALID = "Value must be between {min} and {max}";

    // IDENTIFIER
    public static final String UUID_NOT_VALID = "Must be a valid UUID";

    // EMAIL
    public static final String EMAIL_NOT_VALID = "Email must contain any numbers, letters or RFC 5322 allowed characters followed by an @ symbol and any number of domain names separated by dots (e.g.: 'some.valid_name@thirdlevel.secondlevel.firstlevel')";

    // USER
    public static final String USER_NOT_FOUND_BY_ID = "A user with ID '%s' could not be found";
    public static final String USER_NOT_FOUND_BY_EMAIL = "A user with email '%s' could not be found";
    public static final String USER_ALREADY_EXISTS = "A user with email '%s' already exists";

    // GROUP
    public static final String GROUP_NOT_FOUND = "A group with ID '%s' could not be found";
    public static final String GROUP_ALREADY_EXISTS = "A group with name '%s' already exists";

    // EXPENSE
    public static final String EXPENSE_NOT_FOUND = "An expense with ID '%s' could not be found";
}
