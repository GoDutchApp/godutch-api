package com.gomezortiz.godutch.domain.user;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository {
    Optional<User> findById(UUID id);
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
    User create(User user);
    User update(User user);
    void delete(User user);
}
