package com.gomezortiz.godutch.domain.user;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Value
@Builder
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    UUID id;

    @With
    @Column(name = "first_name", nullable = false)
    String firstName;

    @With
    @Column(name = "last_name_1", nullable = false)
    String lastName1;

    @With
    @Column(name = "last_name_2")
    String lastName2;

    @With
    @Column(name = "email", nullable = false, unique = true)
    String email;

    public static User create(UUID id, String firstName, String lastName1, String lastName2, String email) {
        return new User(id, firstName, lastName1, lastName2, email);
    }
}
