package com.gomezortiz.godutch.infrastructure.group;

import com.gomezortiz.godutch.domain.group.Group;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;
import io.micronaut.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface GroupJpaRepository extends JpaRepository<Group, UUID>, JpaSpecificationExecutor<Group> {
    Optional<Group> findByName(String name);
}
