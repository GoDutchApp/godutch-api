package com.gomezortiz.godutch.infrastructure.expense;

import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExpenseJpaRepository extends JpaRepository<Expense, UUID> {
    List<Expense> findByGroup(Group group);
}
